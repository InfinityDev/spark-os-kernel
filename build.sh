PREFIX=i686-elf-
CXX=$PREFIX"g++"
CXXFlags="-std=c++0x -Wall -Wextra -m32 -ffreestanding -fno-exceptions -fno-rtti -O3 -Iinclude"
LD=$PREFIX"ld"
PATH=$PATH:~/opt/cross/bin


$CXX arch/generic/drivers/console.cxx -o build/console.o -c $CXXFlags
$CXX arch/x86/boot/main.cxx -o build/x86_main.o -c $CXXFlags
$CXX arch/x86/drivers/vgascreen.cxx -o build/x86_vgascreen.o -c $CXXFlags
cpp arch/x86/boot/boot.s -o arch/x86/boot/boot_tmp.s -Iinclude
$CXX -m32 arch/x86/boot/boot_tmp.s -o build/x86_boot.o -c
rm arch/x86/boot/boot_tmp.s

$LD build/x86_boot.o build/x86_main.o build/console.o build/x86_vgascreen.o -o build/kernel.bin -T build/linker.ld

bash build/mkiso.sh
