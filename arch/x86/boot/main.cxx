#include <vgacolour.hxx>
#include <vgascreen.hxx>
#include <console.hxx>
void printd(int num) {
	do {
		Console::printc(num % 10 + '0');
		num /= 10;
	} while (num > 0);
}

void printCoords() {
	int x = Console::getXPosition();
	int y = Console::getYPosition();
	printd(x);
	Console::println("");
	printd(y);
}

extern "C" int main() {
	Console::setForeground(WHITE);
	Console::setBackground(BLACK);

	Console::println("");
	Console::print("hello, world ");
	Console::scroll();
	return 0;
}


