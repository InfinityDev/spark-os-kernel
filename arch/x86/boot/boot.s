#define ASM_FILE
#define MULTIBOOT_CHECKSUM
#include "multiboot.hxx"


#define STACK_SIZE 0x4000

#define MULTIBOOT_HEADER_FLAGS		0x00000003

.section .text


	
.align 4
multiboot_header:
	.long MULTIBOOT_HEADER_MAGIC
	.long MULTIBOOT_HEADER_FLAGS
	.long -(MULTIBOOT_HEADER_MAGIC + MULTIBOOT_HEADER_FLAGS)
	
	.long multiboot_header
	.long start
	.long bss
	.long end
	.long start
.globl start, _start
_start:
start:
movl $(stack + STACK_SIZE), %esp
	
	pushl $0
	popf
	
	pushl %ebx
	pushl %eax
	
	call main
	
	
loop:	hlt
		jmp loop
		
halt_message:
	.asciz "Halted."
stack:
	.skip STACK_SIZE
