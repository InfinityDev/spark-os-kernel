#include <x86/io.hxx>
#include <vgascreen.hxx>

void VGAScreen::putc (int x, int y, char c, enum VGAColour bg, enum VGAColour fg) {
	putc ((y * vgawidth) + x, c, bg, fg);
}

void VGAScreen::putc (int i, char c, enum VGAColour bg, enum VGAColour fg) {
	char* ptr = VGAMemory + (i * 2);
	ptr[0] = c;
	ptr[1] = bg << 4 | fg;
}

char VGAScreen::getc (int x, int y) {
	return getc ((y * vgawidth) + x);
}

char VGAScreen::getc (int i) {
	return *(VGAMemory + (i * 2));
}

void VGAScreen::updatecursor (int x, int y) {
	updatecursor( (y * vgawidth) + x);
	
}

void VGAScreen::updatecursor (int i) {
	outb(14, 0x3D4);
	outb((i >> 8) & 0xFF, 0x3D5);
	outb(15, 0x3D4);
	outb(i & 0xFF, 0x3D5);
	
}
