#include <vgacolour.hxx>
#include <vgascreen.hxx>
#include <console.hxx>

int Console::x = 0, Console::y = 0;
enum VGAColour Console::bg = BLACK, Console::fg = WHITE;

void Console::print(const char* content) {
	do {
		if(*content == '\n') {
			setPosition(0, y+1);
		}
		else if (*content == '\0') {
			break;
		}
		else {
			VGAScreen::putc(x, y, *content, bg, fg);	
			setPosition(x + 1, y);
		}
	} while (*++content > 0);
	VGAScreen::updatecursor(x, y);
}
void Console::printc(char c) {
	VGAScreen::putc(x, y, c, bg, fg);
	setPosition(x + 1, y);
}
void Console::Coreprint(const char* content) {
	
}

void Console::println(const char* content) {
	print(content);
	++y;
	x = 0;
}

void Console::setBackground (enum VGAColour colour) {
	bg = colour;
}

void Console::setForeground (enum VGAColour colour) {
	fg = colour;
}

VGAColour Console::getBackground() {
	return bg;
}

VGAColour Console::getForeground() {
	return fg;
}

void Console::setPosition(int xpos, int ypos) {
	while(xpos >= 80) {
		++ypos;
		x -= 80;
	}
	x = xpos;
	y = ypos;
}

int Console::getXPosition() {
	return x;
}

int Console::getYPosition() {
	return y;
}

void Console::clear() {
	for(int i = 0xB8000; i < 0xB8FA0; ++i) {
		*(int*)i = 0;
	}
	x = y = 0;
}
void Console::scroll() {
	for(int i = 0xB8000; i < 0xB8FA0; ++i) {
		*(int*)(i - 160) = *(int*)i;
	}
	VGAScreen::updatecursor(x, --y);
}
