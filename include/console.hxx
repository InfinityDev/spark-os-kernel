#ifndef Console_hh

#define Console_hh
#include "vgacolour.hxx"

class Console {
public:
	static void print(const char *);
	static void printc(char);
	static void println(const char *);
	
	static char read();
	static char * readln();
	
	static void setForeground(enum VGAColour);
	static void setBackground(enum VGAColour);
	static VGAColour getForeground();
	static VGAColour getBackground();
	
	static void setPosition(int, int);
	static int getXPosition();
	static int getYPosition();
	
	static void clear();
	
	static void scroll();
private:
	static int x;
	static int y;
	static enum VGAColour bg, fg;
	static void Coreprint(const char *);
};


#endif
