//If it is not defined already, define VGAColour_hh
#ifndef VGAColour_hh
#define VGAColour_hh

enum VGAColour {
	BLACK = 0x0,
	DARK_BLUE = 0x1,
	GREEN = 0x2,
	CYAN = 0x3,
	CRIMSON = 0x4,
	PURPLE = 0x5,
	GOLD = 0x6,
	GREY = 0x7,
	DARK_GREY = 0x8,
	BLUE = 0x9,
	LIME = 0xa,
	AQUA = 0xb,
	RED = 0xc,
	MAGENTA = 0xd,
	YELLOW = 0xe,
	WHITE = 0xf
};
#endif
