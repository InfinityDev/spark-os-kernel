#ifndef VGAScreen_hh

#define VGAScreen_hh
#include "vgacolour.hxx"

class VGAScreen {
public:
	static void putc (int x, int y, char c, enum VGAColour bg, enum VGAColour fg);
	static void putc (int i, char c, enum VGAColour bg, enum VGAColour fg);
	static char getc (int x, int y);
	static char getc (int i);
	static void updatecursor(int x, int y);
	static void updatecursor(int i);
private:
	static const int vgawidth = 80;
	static constexpr char* VGAMemory = (char*)0xB8000;
};
#endif




